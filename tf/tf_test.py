import pandas as pd
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

import tensorflow as tf

learning_rate = 0.01
training_epochs = 25
batch_size = 200
display_step = 1

f = open("./agaricus.txt.train", "r")
sample_raw = []
feature_set = set()

while True:
    line = f.readline()
    if line is None or line == "":
        break

    temp = line.split(" ")
    label = temp[0]
    feature = dict()
    for i in xrange(1, len(temp)):
        kv = temp[i].split(":")
        feature[kv[0]] = kv[1]
        feature_set.add(kv[0])
        sample_raw.append([label, feature])

feature_list = list(feature_set)
feature_size = feature_set.__len__()
class_size = 1
sample = []
for e in sample_raw:
    label = [float(e[0])]
    feature = []
    for feature_key in feature_list:
        feature.append(float(e[1].get(feature_key, "0")))
    s = np.array(feature + label)
    sample.append(s)

sample_df = pd.DataFrame(np.array(sample))

sample_size = sample.__len__()

x = tf.placeholder(tf.float32, [None, feature_size], name="X")
y = tf.placeholder(tf.float32, [None, class_size], name="Y")

w = tf.Variable(tf.zeros([feature_size, class_size]), name="weight")
b = tf.Variable(tf.zeros(class_size), name="bias")

y_predict = tf.nn.sigmoid(tf.matmul(x, w) + b)

loss = tf.reduce_mean(-tf.reduce_sum(y * tf.log(y_predict)))

optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss)

print "sample count: %d" % sample_size
print "feature size: %d" % feature_size

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    # Training cycle
    loss_list = []
    for epoch in range(training_epochs):
        total_batch = int(sample_size / batch_size)

        loss_value_total = 0
        for i in range(total_batch):
            _, loss_value = sess.run([optimizer, loss],
                                     feed_dict={
                                         x: sample_df[i * batch_size: (i + 1) * batch_size].iloc[:, 0:feature_size],
                                         y: sample_df[i * batch_size: (i + 1) * batch_size].iloc[:,
                                            feature_size:feature_size + 1]})
            loss_value_total += loss_value
        avg_loss = loss_value_total / total_batch
        loss_list.append(avg_loss)
        if (epoch + 1) % display_step == 0:
            print "Epoch: %d, loss=%e" % (epoch + 1, avg_loss)

    y_result = sess.run(y_predict, feed_dict={x: sample_df.iloc[:, 0:feature_size]})
    correct_prediction = tf.equal(tf.argmax(y_result, 1), tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    print "Accuracy: ", (sess.run(accuracy, feed_dict={x: sample_df.iloc[:, 0:feature_size],
                                                       y: sample_df.iloc[:, feature_size:feature_size + 1]}))
    w_value, b_value = sess.run([w, b])
    print w_value
    print b_value
