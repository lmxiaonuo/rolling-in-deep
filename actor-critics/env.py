from time import sleep

import pandas as pd
import numpy as np


class Env:
    def __init__(self, initial_state=None):

        self.states = [1, 2, 3, 4, 5, 6, 7, 8]

        self.terminal_states = dict()
        self.terminal_states[6] = 1
        self.terminal_states[7] = 1
        self.terminal_states[8] = 1

        self.current_state = 1

        # feature of each states
        self.features = dict()
        self.features[1] = np.array([1.0, 0.0, 0.0, 1.0])
        self.features[2] = np.array([1.0, 1.0, 1.0, 0.0])
        self.features[3] = np.array([1.0, 0.0, 0.0, 0.0])
        self.features[4] = np.array([1.0, 0.0, 1.0, 1.0])
        self.features[5] = np.array([1.0, 1.0, 0.0, 0.0])
        self.features[6] = np.array([0.0, 1.0, 1.0, 1.0])
        self.features[7] = np.array([0.0, 1.0, 1.0, 1.0])
        self.features[8] = np.array([0.0, 1.0, 1.0, 1.0])

        # action type
        self.actions = ['UP', 'RIGHT', 'DOWN', 'LEFT']

        self.action_recall = dict()
        self.action_recall[1] = ['RIGHT', 'DOWN']
        self.action_recall[2] = ['RIGHT', 'LEFT']
        self.action_recall[3] = ['RIGHT', 'DOWN', 'LEFT']
        self.action_recall[4] = ['RIGHT', 'LEFT']
        self.action_recall[5] = ['DOWN', 'LEFT']
        self.action_recall[6] = ['UP']
        self.action_recall[7] = ['UP']
        self.action_recall[8] = ['UP']

        # rewards of each action
        self.rewards = dict()
        self.rewards['1#DOWN'] = -1.0
        self.rewards['3#DOWN'] = 1.0
        self.rewards['5#DOWN'] = -1.0

        # state transform matrix
        self.transform = dict()
        self.transform['1#DOWN'] = 6
        self.transform['1#RIGHT'] = 2
        self.transform['2#LEFT'] = 1
        self.transform['2#RIGHT'] = 3
        self.transform['3#DOWN'] = 7
        self.transform['3#LEFT'] = 2
        self.transform['3#RIGHT'] = 4
        self.transform['4#LEFT'] = 3
        self.transform['4#RIGHT'] = 5
        self.transform['5#DOWN'] = 8
        self.transform['5#LEFT'] = 4

        self.gamma = 0.9

    def get_action_recall(self, state):
        return self.action_recall[state]

    def reset(self):
        self.current_state = int(np.random.random(size=1) * 5) + 1
        return False, self.current_state, self.get_state_feature(), self.actions[
            int(np.random.random(size=1) * self.get_action_size())]

    def get_states(self):
        self.states

    def get_gamma(self):
        return self.gamma

    def get_action_size(self):
        return len(self.actions)

    def get_actions(self):
        return self.actions

    def get_current_state(self):
        return self.current_state

    def get_states(self):
        return self.states

    def get_state_feature_size(self):
        return len(self.features[1])

    def get_state_feature(self, state=None):
        if state == None:
            return self.features[self.current_state]
        else:
            return self.features[state]

    # get environment feedback.
    # is terminate, state, state feature, reward.
    def get_env_feedback(self, action):
        state = self.current_state
        is_terminal = False
        r = 0.0

        key = '%d#%s' % (state, action)
        if key in self.rewards:
            r = self.rewards[key]

        if state in self.terminal_states:
            is_terminal = True

        if key in self.transform:
            self.current_state = self.transform[key]

        return is_terminal, self.current_state, self.features[self.current_state], r

    def update_env(S, episode, step_counter):
        return

    def display(self, state, action):
        part_a = []
        for i in xrange(5):
            if self.current_state == i + 1:
                part_a.append('o')
            else:
                part_a.append('+')

        part_b = []
        if self.current_state == 6:
            part_b.append('o')
        else:
            part_b.append('x')

        part_b.append(' ')

        if self.current_state == 7:
            part_b.append('o')
        else:
            part_b.append('t')

        part_b.append(' ')

        if self.current_state == 8:
            part_b.append('o')
        else:
            part_b.append('x')

        print ''.join(part_a)
        print ''.join(part_b)
        print "state:%d, action:%s" % (state, action)
        print ''
        sleep(1)
