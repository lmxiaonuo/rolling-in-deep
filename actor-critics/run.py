#!/bin/python

import sys
import matplotlib.pyplot as plt
from env import *
from env_v2 import *
from rl_brain import *
import numpy as np

if __name__ == "__main__":
    env = EnvV2()
    actor = Actor(env, epsilon=0.001)
    critics = Critic(env, epsilon=0.001)
    actor, critics = sarsa(env, actor, critics, 5000)

    # softmaxpolicy, y = sarsa(grid, evaler, softmaxpolicy, valuepolicy, 5000, 0.05)
    #
    # print(y[len(y) - 1])
    # plt.figure(figsize=(12, 6))
    # plt.plot(y, "-", label="sarsa")
    # plt.xlabel("number of iterations")
    # plt.ylabel("square errors")
    # plt.legend()
    # plt.show()
