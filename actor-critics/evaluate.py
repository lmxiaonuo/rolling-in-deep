#!/bin/python


class Evaluator:
    def __init__(self, env):
        self.env = env
        self.best = dict()
        f = open("./data")
        for line in f:
            line = line.strip()
            if len(line) == 0:
                continue
            temp = line.split(":")
            self.best[temp[0]] = float(temp[1])

    def eval(self, critics):
        env = self.env
        sum = 0.0
        for key in self.best:
            keys = key.split("_")
            s = int(keys[0])
            if s in env.terminal_states:
                continue

            f = env.get_state_feature(s)
            a = keys[1]

            error = critics.get_qvalue(f, a) - self.best[key]
            sum += error * error

        return sum
