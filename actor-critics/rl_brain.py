import numpy as np
from evaluate import *


class Actor:
    def __init__(self, env, epsilon):
        self.env = env
        self.action_size = env.get_action_size()
        self.state_feature_size = env.get_state_feature_size()
        self.theta = [0.0 for i in xrange(self.action_size * self.state_feature_size)]
        self.theta = np.array(self.theta)
        self.theta = np.transpose(self.theta)
        self.epsilon = epsilon
        self.theta_size = len(self.theta)

    def get_theta_size(self):
        return self.theta_size

    def get_f(self, feature, action):
        f = np.array([0.0 for i in xrange(self.theta_size)])
        idx = self.env.get_actions().index(action)
        for i in xrange(self.state_feature_size):
            f[i + idx * self.state_feature_size] = feature[i]

        return f

    def softmax(self, feature, state):
        action_list = self.env.get_actions()
        action_list_size = len(action_list)
        prob = [0.0 for i in xrange(action_list_size)]
        # prob = [0.0 for i in xrange(self.action_size)]
        sum_total = 0.0
        # for i in xrange(self.action_size):
        for i in xrange(action_list_size):
            # f = self.get_f(feature, self.env.get_actions()[i])
            f = self.get_f(feature, action_list[i])
            prob[i] = np.exp(np.dot(f, self.theta))
            sum_total += prob[i]

        # for i in xrange(self.action_size):
        for i in xrange(action_list_size):
            prob[i] /= sum_total

        return prob

    def choose_action(self, feature, state):
        action_list = self.env.get_actions()
        action_list_size = len(action_list)

        prob = self.softmax(feature, state)
        r = np.random.random(size=1)
        s = 0.0
        # for i in xrange(self.action_size):
        for i in xrange(action_list_size):
            s += prob[i]
            if s >= r:
                # return self.env.get_actions()[i]
                return action_list[i]

        # return self.env.get_actions()[self.action_size - 1]
        return action_list[action_list_size - 1]

    def update_theta(self, state, state_feature, action, q_value):
        action_list = self.env.get_actions()
        action_list_size = len(action_list)

        fa = self.get_f(state_feature, action)
        prob = self.softmax(state_feature, state)

        delta_logj = fa
        # for i in xrange(self.action_size):
        for i in xrange(action_list_size):
            # a = self.env.get_actions()[i]
            a = action_list[i]
            f = self.get_f(state_feature, a)
            delta_logj -= f * prob[i]
        self.theta += self.epsilon * delta_logj * q_value


class Critic:
    def get_theta_size(self):
        return self.theta_size

    def __init__(self, env, epsilon):
        self.env = env
        self.action_size = env.get_action_size()
        self.state_feature_size = env.get_state_feature_size()
        self.theta = [0.0 for i in xrange(self.action_size * self.state_feature_size)]
        self.theta = np.array(self.theta)
        self.theta = np.transpose(self.theta)
        self.theta_size = len(self.theta)
        self.epsilon = epsilon

    def get_f(self, feature, action):
        f = np.array([0.0 for i in xrange(self.theta_size)])
        idx = self.env.get_actions().index(action)
        for i in xrange(self.env.get_state_feature_size()):
            f[i + idx * self.env.get_state_feature_size()] = feature[i]
        return f

    def get_qvalue(self, state_feature, action):
        f = self.get_f(state_feature, action)
        return np.dot(f, self.theta)

    def update_theta(self, state_feature, action, target):
        p_value = self.get_qvalue(state_feature, action)
        error = p_value - target
        f = self.get_f(state_feature, action)
        self.theta = self.theta - self.epsilon * error * f - 0.00001 * self.theta

    def get_qmatrix(self):
        head = list('\t') + self.env.get_actions()
        print '\t'.join(head)
        for state in self.env.get_states():
            line = list()
            line.append(str(state))
            for action in self.env.get_actions():
                q_value = self.get_qvalue(self.env.get_state_feature(state), action)
                line.append(str(q_value))
            print '\t'.join(line)


def sarsa(env, actor, critics, iter_num):
    gamma = env.gamma
    # evaluate = Evaluator(env)
    for i in xrange(actor.get_theta_size()):
        actor.theta[i] = 0.0

    for i in xrange(critics.get_theta_size()):
        critics.theta[i] = 0.0

    for iter in xrange(iter_num):
        is_terminal, state, state_feature, action = env.reset()
        count = 0
        while False == is_terminal and count < 100:
            # if iter > 1000:
            #     env.display(env.current_state, action)
            is_terminal, state_next, state_feature_next, r = env.get_env_feedback(action)

            action_next = actor.choose_action(state_feature_next, state_next)
            critics.update_theta(state_feature, action,
                                 r + gamma * critics.get_qvalue(state_feature_next, action_next))

            actor.update_theta(state, state_feature, action, critics.get_qvalue(state_feature, action))
            count += 1
            state = state_next
            state_feature = state_feature_next
            action = action_next

        critics.get_qmatrix()
        # eval = evaluate.eval(critics)
        # print("%d:%.3f" % (iter, eval))
        # print "---------- iteration end ---------- "
    return actor, critics
