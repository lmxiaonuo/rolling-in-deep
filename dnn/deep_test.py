from keras.layers import Dense, Activation
from keras.models import Sequential
import pandas as pd
import numpy as np

table = pd.read_table("trainSet", sep=" ", header=None)
# table = pd.read_table("dataSet", sep=" ", header=None)

print(table.head())

X = table[[0, 1, 2, 3, 4, 5]]
Y = table[[6]]

# X = table[[0, 1, 2]]
# Y = table[[3]]

X = np.asarray(X)
Y = np.asarray(Y)
print(X.shape)
print(Y.shape)

batch_size = 10
model = Sequential()
model.add(Dense(16, input_dim=6, activation='sigmoid'))
model.add(Dense(8, activation='sigmoid'))
model.add(Dense(1, activation='sigmoid'))

model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

model.fit(X, Y, batch_size=batch_size, nb_epoch=100)
scores = model.evaluate(X, Y)
print("%s: %.2f%%" % (model.metrics_names[1], scores[1] * 100))

Y_class = model.predict_classes(X)
Y_predict = model.predict(X)
print(Y_class)
print(Y_predict)
